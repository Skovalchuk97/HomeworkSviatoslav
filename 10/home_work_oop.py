import uuid
import datetime


class BankAccount:
    deposit = 0

    def __init__(self, name: str, start_deposit: int or float, percent: int):
        """
        Creating new bank account for new client
        """
        self.name = name
        self.start_deposit = start_deposit
        self.__percent = percent
        self.creation_date = datetime.date.today()
        self.unique_id = uuid.uuid4()
        BankAccount.deposit += start_deposit

    @property
    def percent(self):
        return self.__percent

    @percent.setter
    def percent(self, new_percent):
        self.__percent = new_percent

    def __str__(self):
        return f'Dear {self.name}, your deposit = {self.start_deposit} and percent rate is {self.__percent}'

    def add_money(self, amount: int):
        """
        Adding money to the current account
        """
        self.start_deposit += amount
        BankAccount.deposit += amount

    def subtract_money(self, amount: int):
        """
        Subtraction money from the current account
        """
        self.start_deposit -= amount
        BankAccount.deposit -= amount

    def __del__(self):
        print(
            f' {self.name},your bank account was closed, in case of liquidation of bank, refund amount is {self.start_deposit}')

    def transfer_money(self, recipient, amount):
        """
        Send money from one account to another
        """
        if recipient.start_deposit > amount:
            self.start_deposit -= amount
            recipient.start_deposit += amount
        else:
            self.start_deposit += recipient.start_deposit
            recipient.start_deposit = 0

    @property
    def get_daily_profit(self):
        return self.start_deposit * self.__percent / 100 / 365

    @staticmethod
    def bank_credo():
        print(f'Our bank is the best bank in the univers, thank you for your choice ')


Alan = BankAccount('Alan', 100_000, 10)
Kim = BankAccount('Kim', 50_000, 5)
Alan.percent = 13
Alan.add_money(100_000)
Kim.subtract_money(25_000)
Alan.transfer_money(Kim, 1000)
