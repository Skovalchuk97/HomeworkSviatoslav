# Hапишіть программу "Касир в кінотеатрі", яка буде виконувати наступне:
# Попросіть користувача ввести свсвій вік.
# - якщо користувачу менше 7 - вивести "Тобі ж <>! Де твої батьки?"
# - якщо користувачу менше 16 - вивести "Тобі лише <>, а це е фільм для дорослих!"
# - якщо користувачу більше 65 - вивести "Вам <>? Покажіть пенсійне посвідчення!"
# - якщо вік користувача містить 7 - вивести "Вам <>, вам пощастить"
# - у будь-якому іншому випадку - вивести "Незважаючи на те, що вам <>, білетів всеодно нема!"
#
# Замість <> в кожну відповідь підставте значення віку (цифру) та правильну форму слова рік
# Для будь-якої відповіді форма слова "рік" має відповідати значенню віку користувача (1 - рік, 22 - роки, 35 - років і тд...).
# Наприклад :
# "Тобі ж 5 років! Де твої батьки?"
# "Вам 81 рік? Покажіть пенсійне посвідчення!"
# "Незважаючи на те, що вам 42 роки, білетів всеодно нема!"
# Зробіть все за допомогою функцій! Не забувайте що кожна функція має виконувати тільки одне завдання і про правила написання коду.

def formatter(age):
    age_int = int(age)
    if age_int == 1:
        return 'рік'
    elif age[-1] == '1':
        if age[-2:] == '11':
            return 'років'
        else:
            return 'рік'
    elif age_int in [12, 13, 14]:
        return 'років'
    elif age[-1] in ['2', '3', '4']:
        return 'роки'
    else:
        return 'років'


def show_age_result(age):
    age_int = int(age)
    age_formatted = formatter(age)

    if age_int < 7:
        print(f'Тобі {age_int} {age_formatted} Де твої батьки?')
    elif age_int < 16:
        print(f'Тобі лише, {age_int} {age_formatted} а це є фільм для дорослих')
    elif age_int > 65:
        print(f'Вам {age_int} {age_formatted} Покажіть пенсійне посвідчення!')
    else:
        print(f'Незважаючи на те, що вам {age_int} {age_formatted} білетів всеодно нема!')
    if '7' in age:
        print('І вам сьогодні пощастить!')


user_input = input('Enter your age: ')
show_age_result(user_input)
