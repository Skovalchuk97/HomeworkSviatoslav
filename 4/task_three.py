# Є стрінг з певним текстом (можна скористатися input або константою).
# Напишіть код, який визначить кількість слів в цьому тексті, які закінчуються на 'о'.

# Текст слід вводити на Українській мові
def last_letter(enter_data: str) -> int:
    symbol = 0
    for i in enter_data.lower().split(" "):
        if i[-1] == "о":
            symbol += 1
    return symbol


user_input = input("Введіть Ваше речення: ")
print(f'Кількість слів в Вашому тексті, які закінчуються на  \'о\' = {last_letter(user_input)} ')

