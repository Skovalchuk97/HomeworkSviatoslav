# завдання 1
# урл http://api.open-notify.org/astros.json
# вивести список всіх астронавтів, що перебувають в даний момент на орбіті
# (дані не фейкові, оновлюються в режимі реального часу)


import requests

url = 'http://api.open-notify.org/astros.json'
response = requests.get(url)
spacemans = response.json()

for spaceman in spacemans['people']:
    print(spaceman['name'])


