# Є два довільних числа які відповідають за мінімальну і максимальну ціну.
# Є Dict з назвами магазинів і цінами:
# { "cito": 47.999, "BB_studio" 42.999, "momo": 49.999,
#   "main-service": 37.245, "buy.now": 38.324, "x-store": 37.166,
#   "the_partner": 38.988, "sota": 37.720, "rozetka": 38.003}.
# Напишіть код, який знайде і виведе на екран назви
# магазинів, ціни яких попадають в діапазон між мінімальною і максимальною ціною.
# Наприклад:
# lower_limit = 35.9
# upper_limit = 37.339
# > match: "x-store", "main-service"

lower_limit = 35.9
upper_limit = 37.339

price_list = {"cito": 47.999,
         "BB_studio": 42.999,
         "momo": 49.999,
         "main-service": 37.245,
         "buy.now": 38.324,
         "x-store": 37.166,
         "the_partner": 38.988,
         "sota": 37.720,
         "rozetka": 38.003}
stores_by_filters = []
for store, price in price_list.items():
    if lower_limit < price < upper_limit:
        stores_by_filters.append(store)
print("Магазини які потрапляють у діапазон заданих цін: " + str(stores_by_filters))
