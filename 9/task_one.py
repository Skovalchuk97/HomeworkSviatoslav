# # напишіть функцію, яка перевірить, чи передане їй число є парним чи непарним (повертає True False)
# #
# # напишіть функцію. яка приймає стрічку, і визначає, чи дана стрічка відповідає результатам роботи методу capitalize()
# # (перший символ є верхнім регістром, а решта – нижнім.) (повертає True False)
# #
# # написати до кожної функції мінімум 5 assert
# #
# # написати декоратор, який добавляє принт з результатом роботи отриманої функції + текстовий параметр,
# # отриманий ним (декоратор з параметром - це там, де три функції)
# #
# # при цьому очікувані результати роботи функції не змінюються (декоратор просто добавляє принт)

def print_foo_result(inform_message):
    def decorator(func):
        def wrapper(*args, **kwargs):
            result = func(*args, **kwargs)
            print(f"{inform_message} is: {result} ")
            return result

        return wrapper

    return decorator


@print_foo_result('Result of paired functions')
def is_paired(arg1: int):
    """
    This functions checks is given argument is even or not
    :param arg1: Any integer data)
    :return: True if arg1 % 2 == 0, else return False
    """
    if arg1 % 2 == 0:
        return True
    else:
        return False


assert is_paired(444) is True
assert is_paired(0) is True
assert is_paired(-10) is True
assert is_paired(0.123) is False
assert is_paired(9) is False


# print(is_paired.__doc__)


@print_foo_result('Result of capitalize function')
def is_capitalize(arg2: str):
    """
    This functions checks if first letter in giver sentence is capital and all another in small case
    :param arg2: Should be string, can be one word or sentence
    :return: Return True if first letter in given sentence is capital and all another in small case. else return false
    """
    if arg2[0].isupper() and arg2[1:].islower():
        return True
    else:
        return False


assert is_capitalize('Valid test') is True
assert is_capitalize('invalid') is False
assert is_capitalize('Invalid as WELL') is False
assert is_capitalize('123') is False
assert is_capitalize('INVALID') is False
assert is_capitalize('0Invalid') is False

# print(is_capitalize.__doc__)
