# Дана довільна строка.
# Напишіть код, який знайде в ній і віведе на екран кількість слів, які містять дві голосні літери підряд.


user_input = input('Введіть любе речення. Тільки на Українській: ').lower().split()
letters_list = ["а", "е", "и", "і", "о", "у", "я", "ю", "є", "ї", "й"]
count_word = 0
for word in user_input:
    count_vowels = 0
    for letter in word:
        if letter in letters_list:
            count_vowels += 1
        else:
            count_vowels = 0
        if count_vowels == 2:
            count_word += 1
            break
print(f'{count_word}  - кількість слів з 2ма голосними поспіль')
