# Зформуйте строку, яка містить певну інформацію про символ в відомому слові.
# Наприклад "The [номер символу] symbol in [тут слово] is '[символ з відповідним порядковим номером]'".
# Слово та номер отримайте за допомогою input() або скористайтеся константою.
# Наприклад (слово - "Python" а номер символу 3) - "The 3 symbol in "Python" is 't' ".

# Word should be in English.
while True:
    user_input = input('Enter your word:')
    if user_input.isdigit():
        print('Enter letter.Try again')
    elif not user_input:
        print('Empty enter. Try again')
    elif user_input.isalpha():
        number = user_input[2:3]
        print(f'The 3 symbol in "{user_input}" is "{number}".')
        break
print('Thank you')
