# Напишіть декоратор, який перетворює результат роботи функції на стрінг
# Напишіть докстрінг для цього декоратора

def decorator(func):
    def wrapper(*args, **kwargs):
        """
        Convert the result to string
        """
        res = str(func(*args, **kwargs))

        return res

    return wrapper


@decorator
def foo(arg1, arg2):
    return arg1, arg2


foo(1, 2)
print(type(foo(1, 2)))
print(foo.__doc__)
