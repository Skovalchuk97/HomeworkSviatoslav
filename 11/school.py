from abc import ABC, abstractmethod
from random import randint
from faker import Faker

name_surname = Faker()


class SchoolStuff(ABC):

    def __init__(self, name, salary):
        self.name = name
        self.salary = salary

    @abstractmethod
    def __str__(self):
        return "It`s fine"


class Teachers(SchoolStuff):
    def __str__(self):
        return f'My name is {self.name}, I am teacher and my salary is {self.salary}'


class TechnicalStaff(SchoolStuff):
    def __str__(self):
        return f'My name is {self.name}, I am technical person  and my salary is {self.salary}'


class School:
    def __init__(self, school_name, principal: Teachers, teachers_list: int = 20, technical_list: int = 5, ):
        self.school_name = school_name
        self.principal = principal
        self.teachers_list = [Teachers(name_surname.name(), randint(6500, 20000)) for i in range(teachers_list)]
        self.technical_list = [TechnicalStaff(name_surname.name(), randint(6500, 10000)) for i in range(technical_list)]

    @property
    def total_salary(self):
        all_employee = []
        all_employee.append(self.principal)
        all_employee += self.teachers_list
        all_employee += self.technical_list
        total_money_employee = sum((obj.salary for obj in all_employee))
        return total_money_employee

    def change_principal(self):
        old_principal = self.principal
        new_principal = self.teachers_list.pop()
        self.principal = new_principal
        self.teachers_list.append(old_principal)

    @staticmethod
    def add_new_teacher(name, salary):
        Teachers(name, salary)


school_one = School('South Park Elementary', Teachers('Homer Simpson', 6000))
print(school_one.teachers_list)
print(school_one.total_salary)
school_one.add_new_teacher('Erick Cartman', 9999)
school_one.change_principal()
